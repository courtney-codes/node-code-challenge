# Questions

## Question 1 

### Explain the output of the following code and why.

```js
    setTimeout(function() {
      console.log("1");
    }, 100);
    console.log("2");
```

### Answer:

This code will output 2, and then 1. `setTimeout` is an asynchronous (non-blocking) function.
This means that the code contained within the callback will execute once the stack is free, not in line order of the source file.
As `setTimeout` is non-blocking, even if the timeout was set to 0, it would still execute once the stack became free.

## Question 2

### Explain the output of the following code and why.

```js
    function foo(d) {
      if(d < 10) {
        foo(d+1);
      }
      console.log(d);
    }
    foo(0);
```

### Answer:

This code will output a countdown of numbers from 10 down to 0. 
In order to understand why, we need to look at what the function is doing, one step at a time.
The first conditional statement checks to see if the argument `d` is less than 10. When we call `foo` with `0`, this is true.
It then calls itself (this is called a _recursive function_), passing in the value of `d` incremented by 1.
It will do this until the first condition evaluates false (which will happen when `d` is `10`.)
It will then `console.log()` the value of `d`, being `10`. Then, as this function has now finished executing, 
the call stack will now finish executing the _previous_ function, where `d` was equal to `9`. `console.log()` is called again,
and this process repeats until the original argument value of `0` is logged.


## Question 3

### If nothing is provided to `foo` we want the default response to be `5`. Explain the potential issue with the following code:

```js
    function foo(d) {
      d = d || 5;
      console.log(d);
    }
```

### Answer:

The issue with this function is that it uses the OR (`||`) logical operator. 
The `||` operator works by evaluating the left hand expression first (in this case, `d`), and if the expression evaluates to false,
it then evaluates the right hand expression. The problem with this is that in Javascript, values can be "truthy" or "falsy".
`undefined` (which would be the value of `d` if foo was called without passing anything) is a falsy value.
However, the number `0`, or a blank string `""`, also evaluate to falsy, and in this case may be a valid argument to pass to `foo`.
You should always be careful when using `||` to set default values. A better alternative in this case would be:

```js
    function foo(d) {
      if (d === undefined) {
        d = 5
      }
      console.log(d);
    }
```

Or the more concise:

```js
    function foo(d) {
      d = d === undefined ? 5 : d
      console.log(d);
    }
```

ES6 also has support for default parameter values:

```js
    function foo(d = 5) {
      console.log(d);
    }
```

## Question 4

### Explain the output of the following code and why.

```js
    function foo(a) {
      return function(b) {
        return a + b;
      }
    }
    var bar = foo(1);
    console.log(bar(2))
```
### Answer:

The output of this function will be `3`. The reason it can do this is because of Javascript's _closures_.
`foo` takes a parameter `a` and returns a function that takes a parameter `b`, and adds `a` and `b` together.
This creates a _closure_, a function within a function that has access to the scope and variables of the outer function.
This means the function `b` has access to the outer variable `a` when it is defined.
When the function returned by `foo` is assigned to the variable `bar`, it still has access to the variable
`a` that was passed to `foo` at the time. Therefore, it can add these two arguments together to produce `3`.

## Question 5

### Explain how the following function would be used:

```js
    function double(a, done) {
      setTimeout(function() {
        done(a * 2);
      }, 100);
    }
```

### Answer

This function, `double`, takes a parameter `a`, and a function `done`, and after 
a short delay (100ms) calls the function passed to it with the first argument doubled.
If we take the example `foo` function from Question 3, which counts down from 10 to the number passed to it,
we could call `double(3, foo)`, and this function would take 3 as an argument, wait 100ms, then call `foo` and pass in
`6`, meaning the output would be `10`, followed by numbers counting down in succession until `6`.

