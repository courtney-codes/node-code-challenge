"use strict";

const sqlite3 = require("sqlite3");
const express = require("express");
const cors = require("cors");

const app = express();
const port = 3000;
const DB_PATH = "./locations.db";

const db = new sqlite3.Database(DB_PATH);

app.get("/locations", cors(), (req, res) => {
  const { q: queryString } = req.query;

  if (queryString.length > 2) {
    let params = { $location: `${queryString}%` };
    let sql = `SELECT name, latitude, longitude FROM locations WHERE name LIKE $location`;

    db.all(sql, params, (error, rows) => {
      if (error) {
        res.status(500).send("There was an error fetching records from the database.");

        throw error;
      }

      res.json(rows);
    });
  }
});

app.listen(port, () => {
  console.log(`Server is now listening at port ${port}`);
});

process.on("SIGTERM", () => {
  db.close();
});
