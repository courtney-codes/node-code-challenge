const path = "http://localhost:3000";

var vm = new Vue({
  el: "#app",
  created() {
    this.debouncedFetch = _.debounce(this.fetchLocations, 750);
  },
  data: {
    test: "foo",
    searchQuery: "",
    locations: [],
  },
  watch: {
    searchQuery(val) {
      this.debouncedFetch(val);
    },
  },
  computed: {
    noResultText() {
      if (this.searchQuery.length <= 2) {
        return "You must enter a search term with at least 2 characters."
      }
      return "No results found for the search term you entered. Please try again."
    }
  },
  methods: {
    async fetchLocations(query) {
      try {
        this.locations = [];
        if (query.length > 2) {
          let results = await fetch(`${path}/locations?q=${query}`);
          let data = await results.json();
          this.locations = data;
        }
      } catch (error) {
        console.log(error);
      }
    },
  },
});
